package Negocio;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SopaDeLetrasTest
{
    public SopaDeLetrasTest()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void crearMatriz() throws Exception
    {
        String palabras="marc,pe,a";
        SopaDeLetras s=new SopaDeLetras(palabras);

        char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};
        char esperado[][]=s.getSopas();
        // :) --> ideal == esperado

        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(true,pasoPrueba);

    }

    @Test
    public void crearMatriz_conError() throws Exception
    {
       
        SopaDeLetras s=new SopaDeLetras();

        char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};
        char esperado[][]=s.getSopas();
        // :) --> ideal == esperado

        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(false,pasoPrueba);

    }

    @Test
    public void ingresarMatriz_Vacia() throws Exception
    {
        String palabras=null;
        SopaDeLetras s=new SopaDeLetras();

        boolean rta=false;

        if(palabras==null)
            rta= true;

        assertEquals(true, rta);    
    }

    @Test
    public void ingresarMatriz_Llena() throws Exception
    {
        String palabras=null;
        SopaDeLetras s=new SopaDeLetras();

        boolean rta=false;

        if(palabras!=null)
            rta= false;
            
        assertEquals(false, rta);    
    }

    @Test
    public void testCuadrada () throws Exception
    {

        String palabras="ANDRE,ANDRE,ANDRE,ANDRE,ANDRE";
        SopaDeLetras s=new SopaDeLetras(palabras);

        boolean rta=s.esCuadrada();

        assertEquals(true, rta); 

    }

    @Test
    public void testNoCuadrada () throws Exception
    {

        String palabras="UN,U,U";
        SopaDeLetras s=new SopaDeLetras(palabras);

        boolean rta=s.esCuadrada();

        assertEquals(false, rta); 

    }
    @Test
    public void testCuadrada2() throws Exception
    {

        String palabras="ANDRESFARI,ANDRESFARI,ANDRESFARI,ANDRESFARI,ANDRESFARI,ANDRESFARI,ANDRESFARI,ANDRESFARI,ANDRESFARI,ANDRESFARI";
        SopaDeLetras s=new SopaDeLetras(palabras);

        boolean rta=s.esCuadrada();

        assertEquals(true, rta); 

    }
     @Test
    public void testNoCuadrada2() throws Exception
    {

        String palabras="ADARME,MARCO,ANTONIO";
        SopaDeLetras s=new SopaDeLetras(palabras);

        boolean rta=s.esCuadrada();

        assertEquals(false, rta); 

    }

    @Test
    public void testRectangular () throws Exception
    {

        String palabras="farid,andre,cinco";
        SopaDeLetras s=new SopaDeLetras(palabras);

        boolean rta=s.esRectangular();

        assertEquals(true, rta); 

    }

    @Test
    public void testNoRectangular () throws Exception
    {

        String palabras="ana,farid,ll";
        SopaDeLetras s=new SopaDeLetras(palabras);

        boolean rta=s.esRectangular();

        assertEquals(false, rta); 

    }

    @Test
    public void testDispersa () throws Exception
    {

        String palabras="FARID,FARI,FAR,F";
        SopaDeLetras s=new SopaDeLetras(palabras);

        boolean rta=s.esDispersa();

        assertEquals(true, rta); 

    }

    
    @Test
    public void testNoDispersa () throws Exception
    {

        String palabras="FARID,FARID,FARID,FARID,FARID";
        SopaDeLetras s=new SopaDeLetras(palabras);

        boolean rta=s.esRectangular();

        assertEquals(false, rta); 

    }
    
    @Test
    public void testDispersa2() throws Exception
    {
        String palabras="ANDRES,FARID,MONDRAGON,ADARME";
        SopaDeLetras s=new SopaDeLetras(palabras);
        boolean rta=s.esDispersa();
         assertEquals(true, rta); 
        
    }
    @Test
    public void testNoDispersa2() throws Exception
    {
        String palabras="ANDRES,ANDRES,ANDRES";
        SopaDeLetras s=new SopaDeLetras(palabras);
        boolean rta=s.esDispersa();
         assertEquals(false, rta); 
        
    }

    @Test
    public void testDiagonalPrincipal() throws Exception
    {

        String palabras="FARID,FARID,FARID,FARID,FARID";
        SopaDeLetras s=new SopaDeLetras(palabras);

        char ideal[]={'F','A','R','I','D'};
        char esperado[]=s.getDiagonalPrincipal();
        boolean rta=sonIgualesVector(ideal,esperado);
        assertEquals(true, rta); 

    }

    @Test
    public void test_Mal_DiagonalPrincipal() throws Exception
    {

        String palabras="ert,rte,fer";
        SopaDeLetras s=new SopaDeLetras(palabras);

        char ideal[]={'F','A','R','I','D'};
        char esperado[]=s.getDiagonalPrincipal();
        boolean rta=sonIgualesVector(ideal,esperado);
        assertEquals(false, rta); 
    }
    @Test
    public void testDiagonalPrincipal2() throws Exception
    {
        String palabras="ANDRES,ANDRES,ANDRES,ANDRES,ANDRES,ANDRES";
        SopaDeLetras s=new SopaDeLetras(palabras);
        char ideal[]={'A','N','D','R','E','S'};
        char esperado[]=s.getDiagonalPrincipal();
        boolean rta=sonIgualesVector(ideal,esperado);
        assertEquals(true, rta); 
        
        
        
    }
    @Test
    public void test2_Mal_DiagonalPrincipal() throws Exception
    {
     String palabras="ANDRES,ADARME,ANDRES,NUMERO,ADARME,ANDRES";
     SopaDeLetras s=new SopaDeLetras(palabras);
     char ideal[]={'A','N','D','R','E','S'};
     char esperado[]=s.getDiagonalPrincipal();
     boolean rta=sonIgualesVector(ideal,esperado);
     assertEquals(false, rta); 
    }

    private boolean sonIguales(char m1[][], char m2[][])
    {

        if(m1==null || m2==null || m1.length !=m2.length)
            return false; 

        int cantFilas=m1.length; 

        for(int i=0;i<cantFilas;i++)
        {
            if( !sonIgualesVector(m1[i],m2[i]))
                return false;
        }

        return true;
    }

    private boolean sonIgualesVector(char v1[], char v2[])
    { 
        if(v1.length != v2.length)
            return false;

        for(int j=0;j<v1.length;j++)
        {
            if(v1[j]!=v2[j])
                return false;
        }
        return true;
    }

}
