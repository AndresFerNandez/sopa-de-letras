package Vista;
import Negocio.*;
import java.util.*;
public class Test_SopaLetra
{

    public static void main(String nada[])
    {

        try{
            SopaDeLetras s=new SopaDeLetras("ANDRES,ANDRES,ANDRES,ANDRES,ANDRES,ANDRES");

            System.out.println(s);
            System.out.println("*******************");
            System.out.println(s.toString2());
            System.out.println("F : "+s.filaMatrizSopas+"C : "+s.columMatrizSopas);

            //Imprime Cuadrada
            if (s.esCuadrada())   
            {
                System.out.println("¿Es Cuadrada?: "+ s.esCuadrada());
                System.out.println("La diagonal principal es:");
                System.out.print("");
                System.out.println(s.getDiagonalPrincipal());

            } 
            else
            {
                System.out.println("¿Es Cuadrada?: "+ s.esCuadrada());
            }
            //Imprime Rectangular
            if (s.esRectangular())   
            {
                System.out.println("¿Es Rectangular?: "+ s.esRectangular());
            } 
            else
            {
                System.out.println("¿Es Rectangular?: "+ s.esRectangular());
            }
            //Imprime Dispersa
            if (s.esDispersa())   
            {
                System.out.println("¿Es Dispersa?: "+ s.esDispersa());
            } 
            else
            {
                System.out.println("¿Es Dispersa?: "+ s.esDispersa());
            }
            
        }catch(Exception error)
        {
            System.err.println(error.getMessage());
        }
    }
}
